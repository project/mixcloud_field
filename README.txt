CONTENTS OF THIS FILE
---------------------
 
 * Introduction
 
 This module adds a mixcloud field to your drupal field types.
 You can add a mixcloud url, and it will render as a mixcloud widget. 
 
 * Requirements

 You need a clean drupal installation.
 You need to have a contenttype.
 
 * Installation
 
 1. install the module
 2. Add a mixcloud field to your contenttype
 3. check the display settings of your newly created field.
 4. Add content to your contenttype and enter a mixcloud url.
 5. Lookls like music in your ears :)
 
 
 * Configuration
 
 Add a mixcloud field  to a contenttype.
 In the next settings screen all the options will be listed.
 
 * Troubleshooting
 
 See Maintainers.
 
 * FAQ
 
 None asked yet.
 
 * Maintainers
 Wouters Frederik (drupal_sensei)
 https://drupal.org/user/721548
 
